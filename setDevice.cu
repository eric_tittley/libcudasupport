#include <stdio.h>

#include "cudasupport.h"

void setDevice(unsigned int const deviceIndex) {
  bool const TerminateOnCudaError = true;
  /* First check that we have enough devices */
  int deviceCount;
  cudaError_t err = cudaGetDeviceCount(&deviceCount);
  checkCudaAPICall(err, __FILE__, __LINE__, TerminateOnCudaError);

  if (deviceCount == 0) {
    printf("ERROR: %s: %i: No CUDA-capable devices available\n",
           __FILE__,
           __LINE__);
    exit(EXIT_FAILURE);
  }

  if (deviceIndex >= deviceCount) {
    printf("ERROR: %s: %i: CudaDevice = %u, available range is %u to %u\n",
           __FILE__,
           __LINE__,
           deviceIndex,
           0,
           deviceCount - 1);
    exit(EXIT_FAILURE);
  }

  /* Specify which GPU we want to use based on the given parameter */
  err = cudaSetDevice(deviceIndex);
  checkCudaAPICall(err, __FILE__, __LINE__, TerminateOnCudaError);
  printf("Cuda device %u set successfully\n", deviceIndex);

  /* Grab the properties of the chosen GPU and print to output */
  printGpuInfo(deviceIndex);
}
