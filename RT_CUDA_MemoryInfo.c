#include <stdio.h>
#include <stdlib.h>

#include "cudasupport.h"

/* Return the smallest amount of free memory on any device */
void RT_CUDA_MemoryInfo(size_t* const free, size_t* const total) {
  int device;
  for (device = 0; device < max_cuda_devs; device++) {
    size_t temp_free;
    size_t temp_total;
    cudaSetDevice(cuda_device_ids[device]);
    cudaMemGetInfo(&temp_free, &temp_total);
    if (device == 0 || temp_free < *free) {
      *free = temp_free;
      *total = temp_total;
    }
  }
  checkCUDAErrors("Failed to retrieve memory information", __FILE__, __LINE__);
  return;
}
