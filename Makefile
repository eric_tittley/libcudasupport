# Expected definitions from the calling code:
#  CC
#  CFLAGS
#  CXX
#  CXXFLAGS
#  NVCC
#  NVCCFLAGS
#  NVCCLIBS
#  AR
#  ARFLAGS
#  ROOT_DIR
#  INC_CUDA
#  DEFS

INCS = -I$(ROOT_DIR)/libcudasupport \
       $(INC_CUDA)

SRC_C = checkCUDAErrors.c \
        ReportCudaDevices.c \
        ReportFileError.c \
        RT_CUDA_InitialiseDevices.c \
        RT_CUDA_MemoryInfo.c \
        RT_CUDA_Util_printDeviceProp.c \
        RT_CUDA_Util_PrintDeviceProperties.c

SRC_CPP = checkCudaAPICall.cpp \
          checkCudaKernelLaunch.cpp

SRC_CUDA = printGpuInfo.cu \
	   setDevice.cu \
	   setGridAndBlockSize.cu

OBJS_C    = $(SRC_C:.c=.o)
OBJS_CPP  = $(SRC_CPP:.cpp=.o)
OBJS_CUDA = $(SRC_CUDA:.cu=.o)

OBJS = $(OBJS_C) $(OBJS_CPP) $(OBJS_CUDA)

LIBRARY = libcudasupport.a

all: $(LIBRARY)

$(LIBRARY): $(OBJS) link.o
	$(AR) $(AR_FLAGS) r $(LIBRARY) $(OBJS) link.o

link.o: $(OBJS_CUDA)
	$(NVCC) $(NVCCFLAGS) -dlink $(OBJS_CUDA) -o link.o

.PHONY: clean
clean:
	-rm -f *.o
	-rm -f *~

.PHONY: distclean
distclean: clean
	-rm $(LIBRARY)

.PHONY: indent
indent:
	indent *.c
	indent *.cpp
	indent *.cu

.PHONY: clang-format
clang-format:
	clang-format -i *.c
	clang-format -i *.cpp
	clang-format -i *.cu
	clang-format -i *.h

.SUFFIXES: .o .c .cpp .cu

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<

.cpp.o:
	$(CXX) $(CXXFLAGS) $(DEFS) $(INCS) -c $<

.cu.o:
	$(NVCC) $(NVCCFLAGS) $(DEFS) $(INCS) -dc $<
