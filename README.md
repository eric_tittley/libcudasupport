# libcudasupport

A collection of routines to support CUDA developement.

Required for
 RT_CUDA
 rtsph_cuda
 Manitou (with CUDA support)
 
Maintained by Eric Tittley
