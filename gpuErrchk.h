/* Code taken from a Stack Overflow answer, modified minorly.
 * https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
 *
 * E.g. usage
 *  gpuErrchk( cudaMalloc(&a_d, size*sizeof(int)) );
 */

#define gpuErrchk(ans) \
  { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code,
                      char const* const file,
                      int const line) {
  if (code != cudaSuccess) {
    fprintf(
        stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    exit(code);
  }
}
