#include <iostream>

#include "cudasupport.h"

void checkCudaAPICall(cudaError_t cudaError,
                      const char* file,
                      int line,
                      bool terminate) {
  if (cudaError != cudaSuccess) {
    std::cerr << "**** CUDA API Error ****" << std::endl
              << "File:  " << file << std::endl
              << "Line:  " << line << std::endl
              << "Error: " << cudaGetErrorString(cudaError) << std::endl;

    if (terminate) {
      exit(cudaError);
    }
  }
}
