#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_types.h>
#include <stdio.h>

#include "cudasupport.h"

int ReportCudaDevices() {
  /* Enumerate all devices in the system and retrieve their properties.
   * Based on code from NVIDIA CUDA Programming Guide 3.1. */
  int deviceCount = -1;
  cudaGetDeviceCount(&deviceCount);
  int device;
  for (device = 0; device < deviceCount; device++) {
#ifdef __cplusplus
    cudaDeviceProp deviceProp;
#else
    struct cudaDeviceProp deviceProp;
#endif
    cudaGetDeviceProperties(&deviceProp, device);
    if (device == 0) {
      if (deviceProp.major == 9999 && deviceProp.minor == 9999) {
        printf("There is no device supporting CUDA.\n");
        deviceCount = -1;
      } else if (deviceCount == 1) {
        printf("There is 1 device supporting CUDA\n");
      } else {
        printf("There are %d devices supporting CUDA\n", deviceCount);
      }
    }
    printf("Device %i: %s\n", device, deviceProp.name);
    const long int Mb = 1048576;
    printf(" %i Mb Global, %i bytes Shared\n",
           (int)((long)deviceProp.totalGlobalMem / Mb),
           (int)deviceProp.sharedMemPerBlock);
    printf(" Number of processors = %i\n", deviceProp.multiProcessorCount);
    printf(" Maximum number threads per block = %i\n",
           deviceProp.maxThreadsPerBlock);
    printf(" Clock Rate= %i kHz\n", deviceProp.clockRate);
    printf(" Compute Capability = %i.%i\n", deviceProp.major, deviceProp.minor);
    printf(" Warp size = %i threads\n", deviceProp.warpSize);
  }

  return deviceCount;
}
