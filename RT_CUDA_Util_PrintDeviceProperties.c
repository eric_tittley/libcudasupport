#include "cudasupport.h"

void RT_CUDA_Util_PrintDeviceProperties(int const device) {
#ifdef __cplusplus
  cudaDeviceProp deviceProp;
#else
  struct cudaDeviceProp deviceProp;
#endif
  cudaGetDeviceProperties(&deviceProp, device);
  cudaSetDevice(device);
  RT_CUDA_Util_printDeviceProp(deviceProp);
}
