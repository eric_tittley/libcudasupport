#include <iostream>

#include "cudasupport.h"

cudaError_t checkCudaKernelLaunch(const char* file, int line, bool terminate) {
  /* Check for errors in the launch of the kernel (and residual errors) */
  cudaError_t cudaError_launch = cudaPeekAtLastError();
  checkCudaAPICall(cudaError_launch, file, line, terminate);

#ifdef DEBUG
  /* Check for errors in the kernel itself; must wait until kernel completes.
   * cudaDeviceSynchronize() will return any previous error peek'd at above if
   * the kernel itself ran successfully. However, if the kernel failed, it is
   * likely to be much more useful to return the launch error.
   * All of this is moot if terminate == TRUE */
  cudaError_t cudaError_sync = cudaDeviceSynchronize();
  checkCudaAPICall(cudaError_sync, file, line, terminate);
  if (cudaError_sync != cudaSuccess && cudaError_launch == cudaSuccess) {
    return cudaError_sync;
  }
#endif

  return cudaError_launch;
}
