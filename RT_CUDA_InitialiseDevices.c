#include <stdio.h>
#include <stdlib.h>

#include "cudasupport.h"

int max_cuda_devs = 0;
int* cuda_device_ids = NULL;

int RT_CUDA_InitialiseDevices() {
  // Find total number of devices
  int totaldevs;
  if (cudaGetDeviceCount(&totaldevs) != cudaSuccess) {
    printf("RT_CUDA Error: No CUDA devices found. %s:%i\n", __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  // Find total number of devices that are 2.x compatible and not busy
  // Using a simple memory query to check device status
  int total2x = 0;
  int device;
  for (device = 0; device < totaldevs; device++) {
#ifdef __cplusplus
    cudaDeviceProp deviceProp;
#else
    struct cudaDeviceProp deviceProp;
#endif
    cudaGetDeviceProperties(&deviceProp, device);
    cudaSetDevice(device);
    size_t temp_free;
    size_t temp_total;
    cudaError_t cuda_error = cudaMemGetInfo(&temp_free, &temp_total);
    if (deviceProp.major >= 2 && deviceProp.major < 9999 &&
        cuda_error == cudaSuccess) {
      total2x++;
    }
  }
  if (total2x) {
    printf("RT_CUDA: Found %i 2.x CUDA device(s)\n", total2x);
#ifdef CUDA_MAX_DEVICES
    printf("RT_CUDA: %i devices were requested\n", CUDA_MAX_DEVICES);
    if (total2x > CUDA_MAX_DEVICES) {
      total2x = CUDA_MAX_DEVICES;
    }
#endif
    printf("RT_CUDA: Using %i CUDA device(s)\n", total2x);
  } else {
    printf("RT_CUDA Error: No available CUDA 2.x devices found. %s:%i\n",
           __FILE__,
           __LINE__);
    return EXIT_FAILURE;
  }

  // Create array of device IDs as all cards may not to suitable
  max_cuda_devs = total2x;
  cuda_device_ids = (int*)malloc(sizeof(int) * max_cuda_devs);
  int device2x = 0;
  for (device = 0; device < totaldevs && device2x < max_cuda_devs; device++) {
#ifdef __cplusplus
    cudaDeviceProp deviceProp;
#else
    struct cudaDeviceProp deviceProp;
#endif
    cudaGetDeviceProperties(&deviceProp, device);
    cudaSetDevice(device);
    size_t temp_free;
    size_t temp_total;
    cudaError_t cuda_error = cudaMemGetInfo(&temp_free, &temp_total);
    if (deviceProp.major >= 2 && deviceProp.major < 9999 &&
        cuda_error == cudaSuccess) {
      cuda_device_ids[device2x] = device;
      device2x++;
      printf("RT_CUDA: Using device %i\n", device);
    }
    RT_CUDA_Util_printDeviceProp(deviceProp);
  }
  if (cudaGetLastError() != cudaErrorDevicesUnavailable) {
    checkCUDAErrors("Error in device discovery", __FILE__, __LINE__);
  }
  return EXIT_SUCCESS;
}
