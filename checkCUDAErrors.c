#include <stdio.h>
#include <stdlib.h>

#include "cudasupport.h"

/* Check last CUDA error; print and quit as not recoverable */
void checkCUDAErrors(char const *message, char const *file, int const line) {
  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) {
    // print the CUDA error message and exit
    printf("RT: CUDA error occured at: %s:%i, RT message: %s\n",
           file,
           line,
           message);
    printf("CUDA error: %s\n", cudaGetErrorString(error));
    exit(-1);
  }
  return;
}
