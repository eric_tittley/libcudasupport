#include <stdio.h>

#include "cudasupport.h"

// Print device properties
#ifdef __cplusplus
void RT_CUDA_Util_printDeviceProp(cudaDeviceProp const deviceProp)
#else
void RT_CUDA_Util_printDeviceProp(struct cudaDeviceProp const deviceProp)
#endif
{
  printf("Name:                          %s\n", deviceProp.name);
  printf("Major revision number:         %d\n", deviceProp.major);
  printf("Minor revision number:         %d\n", deviceProp.minor);
  printf("Number of Multiprocessors:     %d\n", deviceProp.multiProcessorCount);
  printf("Maximum threads per processor: %d\n",
         deviceProp.maxThreadsPerMultiProcessor);
  printf("Maximum threads per block:     %d\n", deviceProp.maxThreadsPerBlock);
  size_t i;
  for (i = 0; i < 3; ++i)
    printf("Maximum dimension %lu of block:  %d\n",
           i,
           deviceProp.maxThreadsDim[i]);
  for (i = 0; i < 3; ++i)
    printf(
        "Maximum dimension %lu of grid:   %d\n", i, deviceProp.maxGridSize[i]);
  printf("Warp size (threads):           %d\n", deviceProp.warpSize);
  printf("Clock rate:                    %d\n", deviceProp.clockRate);
  printf("Total global memory:           %lu\n", deviceProp.totalGlobalMem);
  printf("Total shared memory per block: %lu\n", deviceProp.sharedMemPerBlock);
  printf("Total constant memory:         %lu\n", deviceProp.totalConstMem);
  printf("Total registers per block:     %d\n", deviceProp.regsPerBlock);
  printf("L2 cache size:                 %d\n", deviceProp.l2CacheSize);
  printf("Can map host memory?           %s\n",
         (deviceProp.canMapHostMemory ? "Yes" : "No"));
  printf("Unified Virtual Address Space? %s\n",
         (deviceProp.unifiedAddressing ? "Yes" : "No"));
  printf("ECC Memory?                    %s\n",
         (deviceProp.ECCEnabled ? "Yes" : "No"));
  printf("Maximum memory pitch:          %lu\n", deviceProp.memPitch);
  printf("Memory Bus Width:              %d\n", deviceProp.memoryBusWidth);
  printf("Memory Clock Speed:            %d\n", deviceProp.memoryClockRate);
  printf("Concurrent copy and execution? %s\n",
         (deviceProp.deviceOverlap ? "Yes" : "No"));
  printf("Kernel execution timeout?      %s\n",
         (deviceProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
  return;
}
/* Properties not reported:
 * int 	asyncEngineCount
 * int 	computeMode
 * int 	concurrentKernels
 * int 	integrated
 * int 	maxSurface1D
 * int 	maxSurface1DLayered [2]
 * int 	maxSurface2D [2]
 * int 	maxSurface2DLayered [3]
 * int 	maxSurface3D [3]
 * int 	maxSurfaceCubemap
 * int 	maxSurfaceCubemapLayered [2]
 * int 	maxTexture1D
 * int 	maxTexture1DLayered [2]
 * int 	maxTexture1DLinear
 * int 	maxTexture2D [2]
 * int 	maxTexture2DGather [2]
 * int 	maxTexture2DLayered [3]
 * int 	maxTexture2DLinear [3]
 * int 	maxTexture3D [3]
 * int 	maxTextureCubemap
 * int 	maxTextureCubemapLayered [2]
 * int 	maxThreadsDim [3]
 * int 	pciBusID
 * int 	pciDeviceID
 * int 	pciDomainID
 * size_t 	surfaceAlignment
 * int 	tccDriver
 * size_t 	textureAlignment
 * size_t 	texturePitchAlignment
 */
