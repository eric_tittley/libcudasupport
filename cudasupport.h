#ifndef _CUDASUPPORT_H_
#define _CUDASUPPORT_H_

#include "cuda_runtime.h"

#ifndef __cplusplus
typedef unsigned char bool;
#endif

#ifdef __cplusplus
extern "C" {
#endif

void checkCUDAErrors(char const* message, char const* file, int const line);

int ReportCudaDevices();

void ReportFileError(const char* filename);

extern int max_cuda_devs;
extern int* cuda_device_ids;

int RT_CUDA_InitialiseDevices();

void RT_CUDA_MemoryInfo(size_t* const free, size_t* const total);

/* #ifdef __cplusplus
 * void
 * RT_CUDA_Util_printDeviceProp(cudaDeviceProp const deviceProp);
 * #else
 */
void RT_CUDA_Util_printDeviceProp(struct cudaDeviceProp const deviceProp);
/* #endif */

void RT_CUDA_Util_PrintDeviceProperties(int const device);

#ifdef __cplusplus
}
#endif

void checkCudaAPICall(cudaError_t cudaError,
                      const char* file,
                      int line,
                      bool terminate);

cudaError_t checkCudaKernelLaunch(const char* file, int line, bool terminate);

void printGpuInfo(unsigned int const deviceIndex);

void setDevice(unsigned int const deviceIndex);

int setGridFromBlockSize(size_t const nLoopElements,
                         void* KernelPointer,
                         int blockSize);

void setGridAndBlockSize(size_t const nLoopElements,
                         void* KernelPointer,
                         int* gridSize,
                         int* blockSize);

#define ERT_CUDA_CHECK(x) checkCudaAPICall((x), __FILE__, __LINE__, true)

#ifdef __CUDACC__
#  define ERT_HOST   __host__
#  define ERT_DEVICE __device__
#  define ERT_GLOBAL __global__
#  define ERT_HOST_DEVICE __host__ __device__
#else
#  define ERT_HOST
#  define ERT_DEVICE
#  define ERT_GLOBAL
#  define ERT_HOST_DEVICE
#endif

#endif
